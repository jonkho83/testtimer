//
//  main.m
//  TestTimer
//
//  Created by Jonathan Kho on 26/6/14.
//  Copyright (c) 2014 Jonathan Kho. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JKAppDelegate class]));
    }
}
