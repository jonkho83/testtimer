//
//  JKViewController.h
//  TestTimer
//
//  Created by Jonathan Kho on 26/6/14.
//  Copyright (c) 2014 Jonathan Kho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKViewController : UIViewController

@property (nonatomic, strong) UIButton *UIButtonStart;
@property (nonatomic, strong) NSDate *startDate, *pauseStart, *previousFireDate;
@property (nonatomic, strong) NSTimer *timerDown, *timer;
@property (strong, nonatomic) IBOutlet UILabel *UILabelTimer;


- (IBAction)UIButtonStart:(UIButton *)sender;

- (IBAction)UIButtonStop:(UIButton *)sender;

- (IBAction)UIButtonPause:(UIButton *)sender;

- (IBAction)UIButtonResume:(UIButton *)sender;
@end
