//
//  JKAppDelegate.h
//  TestTimer
//
//  Created by Jonathan Kho on 26/6/14.
//  Copyright (c) 2014 Jonathan Kho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
