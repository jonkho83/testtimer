//
//  JKViewController.m
//  TestTimer
//
//  Created by Jonathan Kho on 26/6/14.
//  Copyright (c) 2014 Jonathan Kho. All rights reserved.
//

#import "JKViewController.h"

@interface JKViewController ()

@end

@implementation JKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)countUp
{   int seconds, minutes, hours;
    
    seconds += 1;
    
    if (seconds == 60)
    {
        seconds = 0;
        minutes++;
        
        if (minutes == 60)
        {
            minutes = 0;
            hours++;
        }
    }
}

- (void)countDown
{   int seconds, minutes, hours;
    
    seconds += 1;
    
    if (seconds == 60)
    {
        seconds = 0;
        minutes++;
        
        if (minutes == 60)
        {
            minutes = 0;
            hours++;
        }
    }
}

/*
- (void)stopTimer
{
    [self.stopWatchTimer invalidate];
    self.stopWatchTimer = nil;
    
}
*/





- (IBAction)UIButtonStart:(UIButton *)sender {
    if ([self.UIButtonStart.titleLabel.text isEqualToString:@"Start"])
    {
        [self.UIButtonStart setTitle:@"Pause" forState:UIControlStateNormal];
        [self.UIButtonStart setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countUp) userInfo:nil repeats:YES];
        self.timerDown = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
        //No NSDate was used here.
        //test
        
        
    } else if ([self.UIButtonStart.titleLabel.text isEqualToString:@"Pause"])
    {
        [self.UIButtonStart setTitle:@"Resume" forState:UIControlStateNormal];
        [self.UIButtonStart setTitleColor:[UIColor colorWithRed:0/255 green:0/255 blue:255/255 alpha:1.0] forState:UIControlStateNormal];
        
        self.pauseStart = [[NSDate dateWithTimeIntervalSinceNow:0] init];
        self.previousFireDate = [[self.timer fireDate] init];
        [self.timer setFireDate:[NSDate distantFuture]];
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"mm:ss"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
        
        NSString *stringFromDate = [formatter stringFromDate:self.pauseStart];
        NSString *stringFromDate1 = [formatter stringFromDate:self.previousFireDate];
        
        
        NSLog(@"The time supposed to run at %@", stringFromDate);
        NSLog(@"The time supposed to run at %@", stringFromDate1);
        
        self.UILabelTimer.text = stringFromDate;
   }
}

- (IBAction)UIButtonPause:(UIButton *)sender {
    
    }

- (IBAction)UIButtonResume:(UIButton *)sender {
    
    }


- (IBAction)UIButtonStop:(UIButton *)sender {
}

@end
